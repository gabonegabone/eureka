package ru.redcollar.eureka_registryserver.config;

import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Configuration;

@EnableEurekaServer
@Configuration
public class MainConfig {
}
